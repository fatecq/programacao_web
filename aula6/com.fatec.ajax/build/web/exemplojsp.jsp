<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>

        <%
            String a = request.getParameter("fname");
            String b = request.getParameter("lname");
            System.out.println(a);
            System.out.println(b);
        %>
        <h1><%=a%></h1>
        <h1><%=b%></h1>
        <h1></h1>
    </body>
</html>
