<%-- 
    Document   : subject2
    Created on : 20 de mai. de 2022, 20:20:37
    Author     : gabri
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Reading checkbox data filled in form</h1>
        <ul>
            <li><p>
                    <b>Maths checkbox:</b>
                    <%= request.getParameter("Maths")%>
                </p></li>
            <li><p>
                    <b>Physics checkbox:</b>
                    <%= request.getParameter("Physics")%>
                </p></li>
        </ul>
    </body>
</html>
