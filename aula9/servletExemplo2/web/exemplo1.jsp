<%-- 
    Document   : exemplo1
    Created on : 13 de mai. de 2022, 19:53:03
    Author     : gabriel
--%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="cliente" scope="application" class="bean.Cliente"/>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:setProperty name="cliente" property="codigo" value="3"/>
        <jsp:setProperty name="cliente" property="nome" value="Maria"/>
        <jsp:setProperty name="cliente" property="idade" value="39"/>
        <%-- comentário em JSP aqui: nossa primeira página JSP --%>

        <h1>Olá mundo</h1>

        <%
            String mensagem = "Bem vindo ao sistema de agenda do FJ-21";
        %>
        <%out.println(mensagem); %>

        <br />

        <%
            String desenvolvido = "Desenvolvido por eu mesmo";
        %>
        <%=desenvolvido%>

        <br />
        
        <%List<Integer> lista = new ArrayList<Integer>();
            lista.add(34);
            lista.add(39);
        %>
        <%=lista.get(0)%>
        
        <br />
        <%=cliente.getNome()%>
    </body>
</html>
