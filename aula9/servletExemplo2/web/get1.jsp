<%-- 
    Document   : get1
    Created on : 20 de mai. de 2022, 19:45:23
    Author     : gabri
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>GET method to read form data using form</h1>
        <ul>
            <li><p><b>Nome:</b>
                        <%= request.getParameter("first_name")%>
                </p></li>
            <li><p><b>Sobrenome:</b>
                        <%= request.getParameter("last_name")%>
                </p></li>
        </ul>
    </body>
</html>
