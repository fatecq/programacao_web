<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<sql:query var="rs" dataSource="jdbc/sakila">
select * from actor
</sql:query>
<style>
    table,th,td{
        border: 3px solid #66ffff;
        border-collapse: collapse;
        border-style: inset;
       
        
    }
    #div1{
        margin: 0 auto;
        width: 50%;
    }
    
</style>
    


<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Listar</title>
    </head>
    <body>
       <div id="div1">
        <h1>Lista de Usuários do SAKILA</h1>
        <table>
            <tr>
                <th>Primeiro Nome</th>
                 <th>Sobrenome</th>
                 <th>Editar</th>
                 <th>Deletar</th>
            </tr>
            
            
       
        
        <c:forEach var="row" items="${rs.rows}">
        <tr>
            <td>${row.first_name}</td>  
            <td>${row.last_name}</td>
            <td><a href="/Aula0306/editarActor?idActor=${row.actor_id}">edit</a></td>
             <td><a href="/Aula0306/deletarActor?idActor=${row.actor_id}">deletar</a></td>
        </tr>    
        </c:forEach>
        </table>
       </div>  
    </body>
</html>
